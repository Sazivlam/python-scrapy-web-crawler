import scrapy
import os
import json
import collections
import telegram


def load_file(file_path):
    if os.path.isfile(file_path):
        return json.load(open(file_path))


def read_old_files(label, file_path):
    old_listings = collections.defaultdict(list)
    if os.path.isfile(file_path):
        data = json.load(open(file_path))
        old_listings[label] = data[label]
    return old_listings


def get_new_listings(response, label, script_selector):
    new_listings = collections.defaultdict(list)

    for script in response.css(script_selector):
        script_text_sanitized = sanitize_string(script.get())
        # Convert string to JSON
        script_json = json.loads(script_text_sanitized)
        listing = {"url": script_json["url"], "price": script_json["offers"]["price"]}
        new_listings[label].append(listing)

    return new_listings


def notify_user(label, listings, bot_token, chat_id):
    if not bot_token or not chat_id:
        print("Missing bot settings. Skipping notifications")
        return

    # Create a bot
    bot = telegram.Bot(token=bot_token)

    # Join all listings to a one string
    message = ' \n\n'.join(
        "🌐 " + listing['url'] + "\n"
        "💵 " + listing['price'] + " DKK"
        for listing in listings)

    # Send a message
    bot.send_message(
        text='New listings have been created for label ' + label + '. Here are all of them: \n\n' + message,
        chat_id=chat_id
    )


# Remove listings that are in the lower part of page. Solves the problem that if one of the listings get deactivated,
# then older one might appear on the first page. We do not care about them, so we remove them from difference list
def clean_difference(new_listings, difference):
    difference_cleaned = []
    for listing in difference:
        # Check if it is in the top part of page
        if new_listings.index(listing) < 10:
            difference_cleaned.append(listing)
    return difference_cleaned


def find_difference_between_lists(l1, l2):
    return [x for x in l2 if x not in l1]


def update_history(history, not_reappeared_listings):
    if not_reappeared_listings:
        history.extend(not_reappeared_listings)
        history_length = len(history)
        # Prevents history file from becoming too big
        if history_length > 100:
            # Removes first (older ones) x elements of history to satisfy restrictions on size
            history = history[(history_length - 100):]
    return history


def rewrite_file(path, content):
    with open(path, 'w') as outfile:
        json.dump(content, outfile, indent=4, sort_keys=True)


def sanitize_string(dirty_string):
    return dirty_string.replace("\n", "").replace("\r", "")


class DbaSpider(scrapy.Spider):
    settings = load_file("settings.json")
    searchings = settings["searchings"]
    script_selector = settings["script_selector"]
    bot_token = settings["bot_token"]
    chat_id = settings["chat_id"]
    notify_about_reappearance = settings["notify_about_reappearance"]
    notify_on_first_crawl = settings["notify_on_first_crawl"]
    name = "dba_listings"
    start_urls = searchings.keys()

    def parse(self, response):
        # Value in searchings dict. Eg. gtx_in_graphics_cards
        label = self.searchings.get(response.url)

        # Path to file
        results_file_path = "results/" + label + ".json"
        # Path to history
        history_file_path = "history/" + label + "-history.json"
        history = load_file(history_file_path)
        # In case file does not exist
        if not bool(history):
            history = []

        # Get old_listings from file
        old_listings = read_old_files(label, results_file_path)

        # Get new_listings
        new_listings = get_new_listings(response, label, self.script_selector)

        # Only if previous files were found
        if bool(old_listings):
            # Finds elements that are in new_listings but are not in old listings and are in the top part of first page
            # Read clean_difference function description for details
            difference = clean_difference(new_listings[label],
                                          find_difference_between_lists(old_listings[label], new_listings[label]))

            if not self.notify_about_reappearance:
                difference = find_difference_between_lists(history, difference)

            # Notify user using telegram only if there are new listings that we care about
            if difference:
                notify_user(label, difference, self.bot_token, self.chat_id)

            history = update_history(history, difference)
        else:
            history = new_listings[label]
            # Notify user using telegram only if he wants it
            if self.notify_on_first_crawl:
                notify_user(label, history, self.bot_token, self.chat_id)

        # Rewrite result files with new content
        rewrite_file(results_file_path, new_listings)

        # Rewrite history file with new content
        rewrite_file(history_file_path, history)
