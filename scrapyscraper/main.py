import json
import os

from twisted.internet.task import LoopingCall
from twisted.internet import reactor

from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings

from spiders.dba_listings import DbaSpider


def load_settings(file_path):
    if os.path.isfile(file_path):
        return json.load(open(file_path))


if __name__ == '__main__':
    settings = load_settings("settings.json")
    configure_logging()

    runner = CrawlerRunner(get_project_settings())
    task = LoopingCall(lambda: runner.crawl(DbaSpider))
    task.start(settings['update_pause_seconds'])
    reactor.run()