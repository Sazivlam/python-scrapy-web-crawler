# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['main.py'],
             pathex=['C:\\Programming Projects\\Hobbies\\New Python Web Scraper\\python-scrapy-web-crawler'],
             binaries=[],
             datas=[],
             hiddenimports=['python-telegram-bot'],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='ListingsNotifier',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None)

import shutil
shutil.copyfile('settings.json', '{0}/settings.json'.format(DISTPATH))
shutil.copytree('results', '{0}/results'.format(DISTPATH))
shutil.copytree('history', '{0}/history'.format(DISTPATH))