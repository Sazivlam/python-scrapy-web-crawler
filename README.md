# Python Scrapy Web Crawler

Simple Python Web Crawler for ebay-like websites, mainly for dba.dk and ss.lv. Idea is to crawl website every x minutes and if new listings appear - notify about it.

## Installation
Requires python, scrappy and python-telegram-bot libraries

To install python you can use this website
https://www.python.org/downloads/

To install scrapy run:
```
pip install scrapy
```
To install python-telegram-bot run:
```
pip install python-telegram-bot --upgrade
```
## Configuration
Before starting script you need to configure it. To do so you need to open `settings.json` in `scrapyscraper` folder.
In that file you can specify:

`searchings` - urls you want to scrape

`script_selector` - selector using which scraper
can find script objects on page that contain listing info

`update_pause_seconds` - delay between each crawl in seconds

`notify_about_reappearance` - used for specifying if you want to get notification about listings that reappeared 
after becoming inactive (e.g. price change)

`notify_on_first_crawl` - used for specifying if you want to get notifications on after the 
first crawl. Setting it to `false` prevents you from getting notifications on the first crawl

Bot information is not required, but then you will not receive any notifications

Telegram Bot settings:

`bot_token` - bot token. How to create bot: https://sendpulse.com/knowledge-base/chatbot/telegram/create-telegram-chatbot

`chat_id` - Telegram chat to which you want to send notifications. How to get chat_id: https://diyusthad.com/2022/03/how-to-get-your-telegram-chat-id.html

## How to run
Option #1:

Run this command to crawl and save results in `results` folder
```
scrapy crawl dba_listings
```

Option #2:

Run `main.py` file. It will schedule scrapy to run once every 120 seconds. Resulting .json files are stored in `results` 
folder in following format
```
{
    "title": [
        {
            "price": "1500",
            "url": "https://www.dba.dk/..."
        },
    ]
}
```
Example:
```
{
    "gtx_in_graphics_cards": [
        {
            "price": "1500",
            "url": "https://www.dba.dk/msi-gtx-1080-ti-gamingx/id-1101855919/"
        }
    ]
}
```

## Packing as single .exe
To pack this program you can use PyInstaller. In the root of this project you can find ```ListingsNotifier.spec``` which
can be used for packing. You can also generate your own .spec file, it is up to you. To pack program you first need to 
make sure that PyInstaller is installed. To install you can use ```pip install pyinstaller```. After that to pack
program you can use ```python -m PyInstaller --clean ListingsNotifier.spec```. The packed program can be found in
```dist``` folder. Run all the commands inside ```scrapyscraper``` folder.

## What to do
- [X] Basic crawling of DBA
- [X] Store crawling results in files and compare new crawl results with previous ones
- [X] Schedule once a minute execution
- [X] Connect Telegram Bot for notifications
- [ ] Basic crawling of SS.lv
- [X] Code optimisations
